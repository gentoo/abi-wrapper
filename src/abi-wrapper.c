/*
 * Copyright 2012 Nathan Phillip Brink <binki@gentoo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <fcntl.h>
#include <fnmatch.h>
#include <libgen.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

static short has(const char *item, const char *list);
static void check_abi(char *argv[], char *canonical, const char *abi, short debug);
static void canonical_execv(char *path, char *canonical, char *argv[], short debug);
static int expand_symlink(char *buf);
static char *minitok(char *list, char **lasts, short *end);
static void minitok_spin(char **lasts, short *end);

#ifndef ABI_WRAPPER_NO_MAIN
/**
 * abi-wrapper binary.
 *
 * \param argc
 *   Number of arguments.
 * \param argv
 *   argv[0] is the name of this binary what we were invoked
 *   as. argv[1] has a full enough pathname to the destination
 *   executable to open() it rather than having to PATH-search
 *   it. argv[2] is the first argument that the program we're going to
 *   invoke will receive (i.e., argv[2] maps onto that program's
 *   argv[1], and argv[1] onto that program's argv[0]).
 */
int main(int argc, char *argv[])
{
  const char hardcoded_abis[] = MULTILIB_ABIS;
  char softcoded_abis[] = MULTILIB_ABIS;
  short debug;

  const char *ABI;
  const char *DEFAULT_ABI;
  const char *ABI_WRAPPER_DEBUG;

  char **exec_argv;
  char *canonical_argv0;

  char *tok;
  char *tok_lasts;
  short tok_end;

  if (argc < 2)
    {
      fprintf(stderr, "%s: Please do not invoke %s directly. This is a wrapper which should be the shebang of a file to which existing binaries are symlinked.\n",
	      PACKAGE_STRING,
	      argv[0]);
      return 1;
    }

  ABI_WRAPPER_DEBUG = getenv("ABI_WRAPPER_DEBUG");
  debug = ABI_WRAPPER_DEBUG
    && strlen(ABI_WRAPPER_DEBUG);

  if (debug)
    fprintf(stderr, "%s: exec=%s\n", PACKAGE_STRING, argv[1]);

  exec_argv = malloc(sizeof(char *) * argc);
  /* argv[1...argc] map onto the program's argv[0...(argc - 1)] */
  memcpy(exec_argv, argv + 1, sizeof(char *) * (argc - 1));
  exec_argv[argc - 1] = NULL;
  canonical_argv0 = exec_argv[0];

  /* Try first the user-requested ABI */
  ABI = getenv("ABI");
  if (ABI && *ABI && has(ABI, hardcoded_abis))
    check_abi(exec_argv, canonical_argv0, ABI, debug);

  /* Try second the user-requested fallback ABI */
  DEFAULT_ABI = getenv("DEFAULT_ABI");
  if (DEFAULT_ABI && *DEFAULT_ABI && has(DEFAULT_ABI, hardcoded_abis))
    check_abi(exec_argv, canonical_argv0, DEFAULT_ABI, debug);

  tok = minitok(softcoded_abis, &tok_lasts, &tok_end);
  while (tok)
    {
      check_abi(exec_argv, canonical_argv0, tok, debug);
      tok = minitok(NULL, &tok_lasts, &tok_end);
    }

  fprintf(stderr, "%s: %s[%s]: abi-wrapper couldn't find an executable for the specified program.", PACKAGE_STRING, argv[0], argv[1]);
  if (ABI)
    fprintf(stderr, " ABI=%s", ABI);
  if (DEFAULT_ABI)
    fprintf(stderr, " DEFAULT_ABI=%s", DEFAULT_ABI);
  fputs(" hardcoded_abis=" MULTILIB_ABIS "\n", stderr);

  return 1;
}
#endif /* ABI_WRAPPER_NO_MAIN */

/**
 * Check if the specified ABI is available for this program and
 * execute it if so.
 */
static void check_abi(char *argv[], char *canonical, const char *abi, short debug)
{
  char *argv0;
  static char path[2*PATH_MAX + 1];
  static char symlink_path[2*PATH_MAX + 1];

  if (snprintf(path, sizeof(path), "%s-%s", argv[0], abi) > (sizeof(path) - 1))
    fprintf(stderr, "%s: [%s]: abi-wrapper attempting to execute binary whose path (%s-%s) is too long for PATH_MAX=%lu",
	    PACKAGE_STRING, argv[0], argv[0], abi, (unsigned long)sizeof(path));
  /*
   * Save argv[0] because canonical_execv() overwrites argv[0]
   * unconditionally. We need the original argv[0] for symlink
   * support.
   */
  argv0 = argv[0];
  canonical_execv(path, canonical, argv, debug);

  /* Handle multicall binaries which use symlinks */
  /* The path to examine for a symlink */
  snprintf(symlink_path, sizeof(symlink_path), "%s", argv0);

  while (1)
    {
      if (expand_symlink(symlink_path))
	return;

      if (snprintf(path, sizeof(path), "%s-%s", symlink_path, abi) > (PATH_MAX - 1))
	fprintf(stderr, "%s: [%s]: abi-wrapper attempting to execute binary whose path (%s-%s) is too long for PATH_MAX=%lu",
		PACKAGE_STRING, argv0, argv0, abi, (unsigned long)sizeof(path));
      canonical_execv(path, canonical, argv, debug);
    }
}

/**
 * \brief
 *   Actually execute the binary, taking into account MULTILIB_BINARIES_NO_CANONICAL.
 *
 * The problem with some programs is that they assume that all
 * symlinks should be followed, always. When a binary is
 * abi-wrappered, this doesn't make sense. First, the abi-wrapper
 * should always be invoked so that the ABI variable (which can
 * _vary_) can be checked for the correct executable. Second, if
 * programs pre-follow symlinks themselves, they end up trying to
 * invoke abi-wrapper directly. abi-wrapper doesn't know how to try to
 * execute itself, so there is the failure.
 *
 * Problem is, not just any programs have this problem. THE program
 * Gentoo users use has it: /usr/lib64/portage/pym/portage/__init__.py:_python_interpreter = os.path.realpath(sys.executable)
 *
 * \param path
 *   The path to the binary.
 * \param canonical
 *   The canonical (user-provided) name for the executable, i.e., what
 *   argv[0] should be set to unless if the binary is in
 *   MULTILIB_BINARIES_NO_CANONICAL.
 * \param argv
 *   The arguments to pass to the binary.
 */
static void canonical_execv(char *path, char *canonical, char *argv[], short debug)
{
  static char multilib_binaries_no_canonical[] = MULTILIB_BINARIES_NO_CANONICAL;
  char *lasts;
  short end;
  char *no_canonical_pattern;

  const char *basename;

  basename = strrchr(canonical, '/');
  if (basename)
    basename ++;
  else
    basename = canonical;

  argv[0] = canonical;
  no_canonical_pattern = minitok(multilib_binaries_no_canonical, &lasts, &end);
  while (no_canonical_pattern)
    {
      if (!fnmatch(no_canonical_pattern, basename, FNM_PATHNAME))
	{
	  /* The pattern matched */
	  if (debug)
	    {
	      fprintf(stderr, "%s: Using non-canonical argv0 (``%s'' instead of ``%s'')\n",
		      PACKAGE_STRING, path, canonical);
	    }
	  argv[0] = path;
	  break;
	}

      no_canonical_pattern = minitok(NULL, &lasts, &end);
    }
  if (debug)
    fprintf(stderr, "%s: execv(%s, {%s, ...}\n", PACKAGE_STRING, path, argv[0]);
  execv(path, argv);
  minitok_spin(&lasts, &end);
}

/**
 * Perform one level of symlink expansion.
 *
 * \param buf
 *   A buffer with a length of at least 2*PATH_MAX + 1 where the
 *   context path is stored and the result will be stored into.
 */
static int expand_symlink(char *buf)
{
  static char raw_symlink[PATH_MAX + 1];
  char *dirname_str;
  char *dest_charptr;
  size_t src_pos;
  ssize_t raw_len;
  size_t updir_count;

  raw_len = readlink(buf, raw_symlink, sizeof(raw_symlink) / sizeof(char) - 1);
  if (raw_len <= 0)
    return 1;

  /*
   * dest_charptr always points to the next char, which starts out
   * being directly after a `/'. Or, for a path relative to the
   * current directory, it points directly to the beginning of buf.
   */
  if (raw_symlink[0] == '/')
    {
      buf[0] = '/';
      dest_charptr = &buf[1];
    }
  else
    {
      dirname_str = dirname(buf);
      if (strcmp(dirname_str, "."))
	{
	  if (dirname_str != buf)
	    strcpy(buf, dirname_str);
	  dest_charptr = buf + strlen(buf);
	  (*dest_charptr++) = '/';
	}
      else
	dest_charptr = buf;
    }

  for (src_pos = 0; src_pos < raw_len; src_pos ++)
    {
      if (!src_pos || raw_symlink[src_pos] == '/')
	{
	  if (src_pos)
	    /* Jump over the '/' if there is one to jump over */
	    src_pos ++;

	  if (raw_symlink[src_pos] == '.')
	    {
	      if (raw_symlink[src_pos + 1] == '.'
		  && (!raw_symlink[src_pos + 2] || raw_symlink[src_pos + 2] == '/'))
		{
		  /* encounterred `/..'<end> or `/../' */
		  /* Go up a dir in buf */
		  updir_count = 1;
		  while (updir_count)
		    {
		      if (dest_charptr > buf)
			{
			  /*
			   * There is at least one directory in buf,
			   * reposition dest_charptr so that it is
			   * removed.
			   */
			  dest_charptr -= 2;
			  while (dest_charptr > buf && *dest_charptr != '/')
			    dest_charptr --;
			  dest_charptr ++;

			  /*
			   * Check if we removed a `../' or a proper
			   * sort of directory.
			   */
			  if (!strncmp(dest_charptr, "../", 3))
			    updir_count ++;
			  else if (!strncmp(dest_charptr, "./", 2))
			    ;
			  else if (*dest_charptr == '/')
			    {
			      /* We are trying to so something like `/../' which is the same as `/' */
			      updir_count = 0;
			      dest_charptr ++;
			    }
			  else
			    updir_count --;
			}
		      else
			{
			  /*
			   * We have hit rock bottom. The only way to
			   * fix updir_count is to add a bunch of our
			   * own `../'.
			   */
			  while (updir_count)
			    {
			      memcpy(dest_charptr, "../", 3);
			      dest_charptr += 3;
			      updir_count --;
			    }
			}
		    }

		  /* skip over this in raw_symlink */
		  src_pos ++;
		  continue;
		}
	      else if (!raw_symlink[src_pos + 1] || raw_symlink[src_pos + 1] == '/')
		{
		  /* encounterred `/.'<end> or `/./' */
		  /* skip over this in raw_symlink */
		  continue;
		}
	    }
	  /* Unskip over `/' */
	  if (src_pos)
	    src_pos --;

	  if (raw_symlink[src_pos] == '/'
	      && dest_charptr > buf
	      && *(dest_charptr - 1) == '/')
	    /* Don't output double forward-slashes */
	    continue;
	}

      /* default fallthrough */
      *(dest_charptr++) = raw_symlink[src_pos];
    }

  *dest_charptr = '\0';

  if (dest_charptr - buf > PATH_MAX)
    return 1;
  return 0;
}

static short has(const char *item, const char *list)
{
  size_t len;

  len = strlen(item);
  while (list)
    {
      while (*list == ' ')
	list ++;

      if (!strncmp(item, list, len)
	  && (list[len] == '\0' || list[len] == ' '))
	return 1;

      list = strchr(list, ' ');
    }

  return 0;
}

/**
 * \brief
 *   Tokenize a whitespace-separated list without damaging it
 *   permanently.
 *
 * \param list
 *   The list to tokenize.
 * \param lasts
 *   A place to store tokenization state.
 * \param end
 *   A second piece of tokenization state.
 */
static char *minitok(char *list, char **lasts, short *end)
{
  char *local_lasts;

  if (list)
    {
      local_lasts = list;
      *end = 0;
    }
  else
    {
      local_lasts = *lasts;
      /* Restore the list so that it can be minitok()ed over again. */
      if (!*end)
	*local_lasts = ' ';
    }
  /*
   * Find the beginning of the next token or discover that there are
   * no more tokens.
   */
  while (*local_lasts == ' ')
    local_lasts ++;
  if (!*local_lasts)
    return NULL;
  list = local_lasts;

  /* Find end of token */
  while (*local_lasts && *local_lasts != ' ')
    local_lasts ++;

  /* Delimit the end of token */
  if (*local_lasts)
    *local_lasts = '\0';
  else
    *end = 1;

  /* Store state reentrantedly */
  *lasts = local_lasts;
  return list;
}

/**
 * \brief
 *   Clean up a list so that it can be minitok()ed through again
 *   instead of being truncated.
 *
 * If a minitok()-based traversal through a list is halted mid-way
 * through and started over, the traversal will never make it past the
 * point where the first traversal stopped. This function restores the
 * whitespace-separated list so that traversal may start over
 * properly.
 *
 * \param lasts
 *   The same lasts argument passed to a prior minitok() call.
 * \param end
 *   The same end argument passed to a prior minitok() call.
 */
static void minitok_spin(char **lasts, short *end)
{
  if (!*end)
    **lasts = ' ';
}
