/*
 * Copyright 2012 Nathan Phillip Brink <binki@gentoo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#undef readlink

/**
 * \brief
 *   Some fake symlinks to test expand_symlink() with.
 */
static ssize_t readlink(const char *path, char *buf, size_t bufsize)
{
  size_t counter;

  const char *symlinks[] = {
    "/usr/bin/qlist", "q",
    "/usr/bin/qlop", "/usr/bin/q",
    "/usr/sbin/q", "../bin/q",
    "q", "../bin/q",
    "qlist", "q",
    "./qlist", "q",
    "../sbin/q", "../bin/q",
    "/usr/bin/q", "../../bin/abi-wrapper",
    "/bin/ls", "abi-wrapper",
    "/libexec/crazy_random", "./../bin/./abi-wrapper",
    "/usr/bin/python2", "python2.6",
    NULL, "",
  };

  for (counter = 0; symlinks[counter] && strcmp(symlinks[counter], path); counter += 2)
    ;
  return snprintf(buf, bufsize, "%s", symlinks[counter + 1]);
}

#define ABI_WRAPPER_NO_MAIN
#include "../src/abi-wrapper.c"

int main(int argc, char *argv[])
{
  size_t counter;

  const char *symlinks_expected[] = {
    "/usr/bin/qlist", "/usr/bin/q",
    "/usr/bin/qlop", "/usr/bin/q",
    "/usr/sbin/q", "/usr/bin/q",
    "q", "../bin/q",
    "qlist", "q",
    "./qlist", "q",
    "../sbin/q", "../bin/q",
    "/usr/bin/q", "/bin/abi-wrapper",
    "/bin/ls", "/bin/abi-wrapper",
    "/libexec/crazy_random", "/bin/abi-wrapper",
    "/usr/bin/python2", "/usr/bin/python2.6",
    NULL, "",
  };

  char buf[2*PATH_MAX + 1];

  for (counter = 0; symlinks_expected[counter]; counter += 2)
    {
      strcpy(buf, symlinks_expected[counter]);
      expand_symlink(buf);
      if (strcmp(buf, symlinks_expected[counter + 1]))
	{
	  fprintf(stderr, "Expected symlink output of ``%s'' for ``%s''; got ``%s'' instead.\n",
		  symlinks_expected[counter + 1], symlinks_expected[counter],
		  buf);
	  return 1;
	}
    }

  return 0;
}
