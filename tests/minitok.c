/*
 * Copyright 2012 Nathan Phillip Brink <binki@gentoo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <string.h>

#define ABI_WRAPPER_NO_MAIN
#include "../src/abi-wrapper.c"

int main(int argc, char *argv[])
{
  size_t counter;
  size_t metacounter;

  char string[] = "1 2 three o'clock, four o'clock rock";
  char *string2;
  const char *tokens[] = {
    "1",
    "2",
    "three",
    "o'clock,",
    "four",
    "o'clock",
    "rock",
    NULL,
  };

  char *tok;
  char *lasts;
  short end;

  string2 = strdup(string);

  for (metacounter = 0; metacounter < 10; metacounter ++)
    {
      for (counter = 0; tokens[counter]; counter ++)
	{
	  tok = minitok(counter ? NULL : string, &lasts, &end);

	  if (!tok)
	    {
	      fprintf(stderr, "[%zu][%zu]: Expected ``%s'', got nothing.\n",
		      metacounter, counter, tokens[counter]);
	      if (metacounter)
		fprintf(stderr, "This may be an indication that minitok_spin() fails.\n");
	      return 1;
	    }

	  if (strcmp(tokens[counter], tok))
	    {
	      fprintf(stderr, "[%zu][%zu]: Expected ``%s'', got ``%s''.\n",
		      metacounter, counter,
		      tokens[counter], tok);
	      return 1;
	    }
	}

      /* The tokenization should reflect being done. */
      if (tok = minitok(NULL, &lasts, &end))
	{
	  fprintf(stderr, "[%zu][%zu]: Expected no token, got token ``%s''.\n",
		  metacounter, counter, tok);
	  return 1;
	}

      /*
       * The point of minitok() is that it shouldn't harm the source
       * string after a complete tokenization round is through.
       */
      if (strcmp(string, string2))
	{
	  fprintf(stderr, "[%zu]: string was modified. Value should be ``%s'', got ``%s''.\n",
		  metacounter, string2, string);
	  return 1;
	}

      /*
       * And minitok_spin() should allow resetting a halted traversal
       * so that it may be started over from the beginning.
       */
      minitok(string, &lasts, &end);
      minitok(NULL, &lasts, &end);
      minitok_spin(&lasts, &end);
    }

  return 0;
}
